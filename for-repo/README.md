# README for librewolf-fedora-ff

## Overview
After using the [prep-librewolf-rpm.sh](https://gitlab.com/bgstack15/librewolf-fedora) script, a modified copy of Fedora Firefox src.rpm git repo exists on the local filesystem.

If this README.md exists in a directory named `for-repo`, then it is still in its source location from the above link, and not in its final location.

## This repository
This README file belongs in the modified copy of that Fedora Firefox src.rpm [git repository](https://src.fedoraproject.org/rpms/firefox). That modified git repo can be sent up to a [new web location](https://gitlab.com/bgstack15/librewolf-fedora-ff).

Fedora [COPR](https://copr.fedorainfracloud.org/coprs/bgstack15/AfterMozilla/packages/) can then use rpkg to pull this new git repository, so users do not have to upload the 400MB src.rpm manually.

## Using this repository
Script `git-helper.sh` included in the [librewolf-fedora](https://gitlab.com/bgstack15/librewolf-fedora) repository will add the new git repo for librewolf-fedora-ff and fix the .gitignore.

## Differences from upstream repo
The Librewolf rpm git repo makes some changes to the Firefox rpm git repo.
Files are added or modified:
* this README.md
* .gitignore
* librewolf.spec
* LibreWolf patches that are not named here
* a few tarballs omitted from upstream git sources:
  * cbindgen-vendor.tar.xz
  * `firefox-langpacks-*.tar.xz`

Files that are removed:
* sources

Fedora's dist-git project provides a lookaside cache for large assets so they do not have to be stored in source control. The `sources` file links to those files, but we embed in the librewolf src.rpm the two tarballs we need.
